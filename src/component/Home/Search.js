import React, { Component } from 'react';
import './Search.css';

const lurl = "https://developerfunnel.herokuapp.com/location";

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            location : ''
        }

    }

    renderCity = (data) => {
        if(data){
            console.log(data);
            return data.map((item) => {
                return(
                    
                    <option value={item.city}>
                        {item.city_name}
                    </option>
                )
            })
        }
    }

    render(){
        return(
            <div className="imageContainer">
                <div id="logo">
                    <b>D!</b>
                </div>
                <div className="heading">
                    Plan Trip With Us
                </div>
                <div className="locationSelector">
                    <select className="locationDropDown">
                        <option>----SELECT CITY----</option>
                        {this.renderCity(this.state.location)}
                    </select>
                    <select className="reataurantsinput">
                        <option>----SELECT HOTEL----</option>
                        
                    </select>
                </div>
            </div>
        )
    }

    componentDidMount(){
        fetch(lurl,{method:'GET'})
        .then((res) => res.json())//return promise
        .then((data) => {this.setState({location:data})})
        .catch((err) => console.log(err))

    }
}

export default Search;
