import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import Home from './Home/Main.js';
import Header from './Header.js';
import Footer from './Footer.js';




const Router = () => {
    return(
        <BrowserRouter>
            <Header/>
            
            <Route exact path="/" component={Home} />
            <Footer/>
        </BrowserRouter>
    )
}

export default Router;
